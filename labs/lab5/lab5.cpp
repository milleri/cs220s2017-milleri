// Izaak Miller
// Lab 5
// Honor Code: The work submitted is my own unless cited otherwise

#include <iostream>
#include <string>
#include "gator.h"

using namespace std;

int main() {

  gator gat("Chompers", "Green");
  gator gat2("Chimpers", "Red");

  cout << "Before setColor() " << gat.getName() << " is " << gat.getColor() << endl;
  cout << "Before setColor() " << gat2.getName() << " is " << gat2.getColor() << endl;

  gat.setColor("Blue");
  gat2.setColor("Gold");

  cout << "After setColor() " <<gat.getName() << " is " << gat.getColor() << endl;
  cout << "After setColor() " <<gat2.getName() << " is " << gat2.getColor() << endl;
}
