// Izaak Miller
// Lab 5
// Honor Code: The work submitted is my own unless cited otherwise

#include <string>

using namespace std;

class gator {
  public:
    gator(string n, string c);
    gator(string n);
    string getName();
    string getColor();
    void setColor(string c);
  private:
    string name;
    string color;
};
