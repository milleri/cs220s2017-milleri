Izaak Miller
Lab 3 February 14th
Honor code: The work submitted is my own unless cited otherwise

This lab was very interesting for me because I discovered that
I am very used to creating arithmetic expressions with infix
notation. The advantage of infix is just that I am more comfortable
with setting up the expression in that way. However, it was easy to
adjust to the prefix expression after practicing with it for a while.
I would say a disadvantage is if you want to add more to the expression
you have to add it to the beginning and cannot add it to the end. Also
you can not use some very popular properties as easily such as the
associative and commutative properties. But an advantage would be it is
easier to see the operation that is occurring before you see the numbers.
From a programming aspect, I imagine it is easier for the computer to
compute an operation if it sees the operator before the numbers. This
would also be similar for the postfix expression. Instead of interrupting
the operation with an operator, it would be easier to load both numbers
either before or after loading the operator. Postfix was the hardest one
for me to get used to. I had to write what I wanted to do first then
write the numbers I wanted to use. This could have become annoying if
I had to do it for a long time. 
