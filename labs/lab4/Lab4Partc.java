//Izaak Miller
//Lab 4 February 28th, 2017
//Honor code: The work submitted is my own unless cited otherwise

public class Lab4Partc {
  public static void main(String[] args) {
    for (int i = 0; i <= 3; i++) {
      for (int j = 0; j <= 3; j++) {
        for(int k = 0; k < 3; k++){
          System.out.println("recur("+i+", "+j+", "+k+") = "+recur(i,j,k));
        }
      }
    }
}
    public static int recur(int i, int j, int k) {
      int answer;
      if(i <= 0 || j <=  0 || k <= 0){ //if any argument equals 0 and the all arugments
        answer = i + j + k;
      } else if(i > j){ // if i > j then answer = i
        answer = i;
      } else if(k > j){ // if k > j then answer = k
        answer = k;
      } else{ //else 3 times all values individually subtracted by one recursively
        answer = 3 * (recur(i-1,j,k) + recur(i,j-1,k) + recur(i,j,k-1));
      }
      return answer; //return answer
    }
}
