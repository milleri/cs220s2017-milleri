// Izaak Miller
// January 31st, 2017
// Lab 1
// Honor code: The work submitted is my own unless cited otherwise


public class Lab1 {
    public static void main(String[] args) {
        int i = 5, j = 6, k = 127, l = 128, m = 255,
            n = 32767, o = 32768;
        System.out.println("i="+i);
        System.out.println("j="+j);
        System.out.println("k="+k);
        System.out.println("l="+l);
        System.out.println("m="+m);
        System.out.println("n="+n);
        System.out.println("o="+o);
    }
}
