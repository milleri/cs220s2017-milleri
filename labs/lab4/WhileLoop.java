//Izaak Miller
//Lab 4 February 28th, 2017
//Honor code: The work submitted is my own unless cited otherwise

public class WhileLoop {
  public void f() {
    int sum = 0;
    int i = 0;
    while (i < 10){
      sum = sum + i;
      i++;
    }
  }
}
