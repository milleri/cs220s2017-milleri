//Izaak Miller
//Lab 4 February 28th, 2017
//Honor code: The work submitted is my own unless cited otherwise

public class Lab4Part4b {
  public static void main(String[] args) {
    for (int i = 0; i <= 5; i++) {
      for (int j = 0; j <= 5; j++) {
        System.out.println("recur("+i+","+j+") = "+recur(i,j));
      }
    }
  }

  public static int recur(int i, int j) {
    int answer;
    if(i <= 0 || j <=  0){ //if i or j equals zero return zero
      answer = 0;
    }else if(i == j){  // if i = j then return i
      answer = i;
    }else if (i > j){  // if i > j then return j
      answer = j;
    }else{  //else 2 * i-1 + j - 1
      answer = 2 * recur(i-1, j) + recur(j-1, i);
    }
      return answer;
  }
}
