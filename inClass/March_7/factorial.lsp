(defun fac (n)
  (cond
    ((<= n 0) 1)           ; fac(n) = 1 if n <= 0
    (t (* n (fac (- n 1)))) ; n * (n - 1)! otherwise
    ))
