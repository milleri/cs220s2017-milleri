// Izaak Miller
// Lab 5
// Honor Code: The work submitted is my own unless cited otherwise

public class Gator {
   private String name;
   private String color;

   // Constructor:
   public Gator(String n, String c) {
      name = n;
      color = c;
   }

   public Gator(String n) {
     name = n;
     color = "Green";
   }

   public String name() { return name; }

   public String color() { return color; }

   public void setColor(String c) { color = c; }
}
