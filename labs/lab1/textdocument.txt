Izaak Miller
Lab 1
Honor code: The work submitted is my own unless cited otherwise


1. They both begin with the public class Lab1 {.
2. There is a section in the byte code called Code. Under this section there
appears to be some machine language commands.
3. They both have public static void main, but the byte code has public static void main(java.lang.String[]);
and the program has public static void main(String[] args) {.

dstore allows you to store a double value into a local variable.
dstore_0 stores a double into a local variable_0. If you try to save an int
constant into a double, you will get an error when trying to run the program.

I believe that the Java compiler uses several different types of instructions
because it makes it easier for the computer to interpret the language. I believe
that it makes it easier for the computer to know where the data is being stored.
I imagine that this method allows the process to be completed faster.

Java byte code for adding two int variables:
iconst_5
iconst_5
iadd
istore_1

Java byte code for multiplying two int variables:
iconst_5
iconst_5
imul
istore_1

Java byte code for subtracting two int variables:
iconst_5
iconst_5
isub
istore_1

Java byte code for division two int variables:
iconst_5
iconst_5
idiv
istore_1

Yes, the compiler does do constant folding. Constant folding
occurs when compiling the file. It converts simple expressions
into the literal or solution in order to save time. The compiler does not
want to do the expression and store in more than one time so this is a faster
way to avoid having to do that. An example program where the constant
folding would occur is below:

public demo{
int x = 7;
int y = x + 4;
int z;

z = y * 3;
return z;
}
