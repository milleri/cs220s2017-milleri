// Izaak Miller
// Lab 5
// Honor Code: The work submitted is my own unless cited otherwise


public class lab5 {
   public static void main(String[] args) {

      Gator gat1 = new Gator("Chompers");
      Gator gat2 = new Gator("Chimpers");

      System.out.println("Before setColor() " + gat1.name()+ " is " + gat1.color());
      System.out.println("Before setColor() " + gat2.name()+ " is " + gat2.color());

      gat1.setColor("Gold");
      gat2.setColor("Blue");

      System.out.println("After setColor() " + gat1.name()+ " is " + gat1.color());
      System.out.println("After setColor() " + gat2.name()+ " is " + gat2.color());

    }
  }
