--add function
add :: Int -> Int -> Int
add n k = n + k--iterative

--subtract function
subtract :: Int -> Int -> Int
subtract n k = n - k--iterative

--multiply function
mult :: Float -> Float -> Float
mult n k = n * k--iterative

--divide function
divide :: Double -> Double -> Double
divide n k | k == 0 = error "cannot divide by zero"--stipulation
divide n k = n / k--iterative

--mod function
remainder :: Int -> Int -> Int
remainder n k = mod n k--iterative

--square root function
root :: Float -> Float
root n | n < 0 = error "cannot take negative square root"--stipulation
root n = sqrt n--iterative

--power function
power :: Int -> Int -> Int
power n k | k < 0 = error "power: negative argument"--stipulation
power n 0 = 1--recursive function
power n k = n * power n (k-1)

--percent function
percent :: Float -> Float
percent n = n / 100

--factorial function
fact :: Int -> Int
fact n = if n == 0--recursive function
         then 1
         else n * fact(n-1)
