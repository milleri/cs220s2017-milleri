--Izaak Miller
--function triples a number less than 100
--if its greater than 100 nothing
--then adds 3 to the end result
tripleSmallNumberAddThree x =
              (if x > 100
              then x
              else x * 3) + 3
