//Izaak Miller
#include <stdio.h>
#include <string.h>

union Stats {
  int b;
  double d;;
};

struct food {
  int num;
  char color[15];
};

int main()
{
  enum TorF {True, False};
  enum Torf answer;
  answer = 1;
  if (answer = 0){
    printf("True");
  }
  if (answer = 1){
    printf("False");
  }
  struct food type = {0}; //Initializing to null

  type.num=4;
  strcpy(type.color, "Orange");

  printf("Number of food is: %d \n", type.num);
  printf(" Color of food is: %s \n", type.color);

  union Stats stats;
  printf( "Memory size occupied by Stats : %lu\n", sizeof(stats));

  return 0;

}
