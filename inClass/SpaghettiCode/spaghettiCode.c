//Izaak Miller ultimate thrilla

#include <stdio.h>

int main(){
  int i = 10;
if (i > 5) goto HERE;
HERE:
printf("I’m here\n");
goto THERE;
WHERE:
printf("If I'm neither here nor there, then where am I?\n");
goto ANYWHERE;
NOR:
printf("I'm neither here nor there\n");
goto WHERE;
THERE:
printf("Now I’m there\n");
goto NOR;
ANYWHERE:
printf("How is anyone anywhere really?\n");
goto FINISH;
FINISH:
printf("My head hurts.\n");
}
