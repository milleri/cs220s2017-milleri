//Izaak Miller
//Lab 4 February 28th, 2017
//Honor code: The work submitted is my own unless cited otherwise

public class Lab4Part4a {
  public static void main(String[] args) {
    for (int i = 0; i <= 20; i++) {
      System.out.println("pow2("+i+") = "+pow2(i));
    }
  }

  public static int pow2(int n) {

    if (n <= 0){ //if n is less than or equal to zero then n = 1;
       n = 1;
    } else{ //else recursively call
      n = 2 * pow2(n - 1);
    }
    return n;
  }
}
