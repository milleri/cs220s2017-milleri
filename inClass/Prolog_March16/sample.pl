%plus
plus(X,Y,Sum):- Sum is X+Y.

%max
max(X,Y,M):- X=<Y, M=Y.
max(X,Y,M):- X>=Y, M=X.

%animals
big(bear).
big(elephant).
small(cat).

brown(bear).
gray(elephant).
black(cat).

dark(Z):- black(Z).
dark(Z):- brown(Z).


%anscrelationship
parent(pam, bob).
parent(tom, bob).
parent(tom, liz).
parent(bob, ann).
parent(bob, pat).
parent(pat, jim).

female(pam).
female(liz).
female(pat).
female(ann).

male(tom).
male(bob).
male(jim).

offspring(X,Y):- parent(Y,X).

mother(X,Y):-parent(X,Y), female(X).

grandparent(X,Z):- parent(X,Y), parent(Y,Z).

sister(X,Y):- parent(Z,X), parent(Z,Y), female(X).

hastwochildren(X):- parent(X,Y), parent(X,Z).

even(X):- 0 is mod(X,2).
odd(X):- 1 is mod(X,2).

isDivisable(X):- (X mod 2)=:= 0 ; (X mod 3)=:=0.

prime(2).
prime(3).
prime(N):- N>1, \+isDivisalbe(N).

allprime(N,X):-
  N > 1
  prime(N),!,
  write(N),
  write('\n'),
    N1 is N - 1,
    allprime(N1, X).

allprime(N,X):-
  N > 1,
  N1 is N-1,
  allprime(N1, X).
