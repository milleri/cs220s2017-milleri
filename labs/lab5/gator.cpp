// Izaak Miller
// Lab 5
// Honor Code: The work submitted is my own unless cited otherwise

#include <string>
#include "gator.h"

using namespace std;

gator::gator(string n, string c) {
  name = n;
  color = c;
}

gator::gator(string n) {
  name = n;
  color = "Green";
}

string gator::getName() {
  return name;
}

string gator::getColor() {
  return color;
}

void gator::setColor(string c) {
  gator::color = c;
}
