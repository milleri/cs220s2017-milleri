--import System.IO
--import System.Directory
--import Data.Char

--getInt :: Char -> Int
--getInt x = digitToInt x

power :: Int -> Int -> Int--method
power n k | k < 0 = error "power: negative argument"--stipulation
power n 0 = 1--base case
power n k = n * power n (k-1)--recursive call

power1 :: Int -> Int -> Int--method
power1 n k | k < 0 = error "power: negative argument"--stipulation
power1 n k = product (take k (repeat n))--use take and repeat to add same number to list

power2 :: Int -> Int -> Int
power2 n k | k < 0 = error "power: negative argument"
power2 n 0 = 1--base case
power2 n k = if mod k 2 < 1 then (n * n) ^ (div k 2) else n * (n^(k-1))--iterative call

prop_powers :: Int -> Int -> Bool--method
prop_powers n k | k < 0 = error "power:negative argument"--stipulation
prop_powers n k = if power n k == power1 n k && power1 n k == power2 n k then True else False--check

--tried to do prop powers in a main function
--main = do
  --  putStrLn "prop_powers"
    --putStrLn "Enter first number"
    --x <- getLine
    --putStrLn "Enter second number"
    --y <- getLine
    --getInt y
    --print(x :: Int)
    --print(y :: Int)
    --power x y
    --power1 x y
    --power2 x y
    --  putStrLn z
      --c = power1 x y
      --putStrLn c
      --d = power2 x y
      --putStrLn d
