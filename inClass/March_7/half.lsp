(defun half (lst)
  (let ((mid (ceiling (length lst) 2)))
    (append (subseq lst mid nil)
            (subseq lst 0 mid))))
